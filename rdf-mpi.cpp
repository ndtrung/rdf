/* Version 0.1
  Pair correlation function between two coordinate sets with the following options:
  - single CPU thread
  - multithreaded CPU threads (OpenMP)
  - GPU-accelerated (CUDA), single GPU
  - MPI with OpenMP
  - MPI without CUDA
  - MPI with CUDA, multiple GPUs
  
  Compile: make -f Makefile.cpu, or 
           make -f Makefile.omp, or 
           make -f Makefile.cuda, or
           make -f Makefile.mpi, or
           make -f Makefile.mpi_omp, or
           make -f Makefile.mpi_cuda

  Usage:   ./calc-rdf      dump.file type1 t1 [type2 t2]
           ./calc-rdf-omp  dump.file type1 t1 [type2 t2] [nthreads n]
           ./calc-rdf-cuda dump.file type1 t1 [type2 t2]
           mpirun -np 6     ./calc-rdf-mpi      dump.file type1 t1 [type2 t2]
           aprun  -n 4 -N 2 ./calc-rdf-mpi-cuda dump.file type1 t1 [type2 t2]

    dump.file  : a LAMMPS dump file (format first 7 columns: id mol type q x y z)
    type1      : atom type of set 1 (-1 for all types)
    type2      : atom type of set 2, optional (default type2=type1) (-1 for all types)
    nthreads   : number of threads for the OpenMP version (default nthreads=1)

  Contact: Trung Dac Nguyen (UChicago) ndtrung@uchicago.edu
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>

#define EPSILON 1e-3
#define DELTA 1000

//#define __USE_SINGLE_CPU
//#define __USE_OPENMP
//#define __USE_MPI_OPENMP
//#define __USE_CUDA
//#define __USE_MPI_NOCUDA
//#define __USE_MPI_CUDA

#if defined(__USE_OPENMP) || defined(__USE_MPI_OPENMP)
#include "omp.h"
#endif

#if defined(__USE_MPI_NOCUDA) || defined(__USE_MPI_CUDA) || defined(__USE_MPI_OPENMP)
#include "mpi.h"
#endif

#if defined(__USE_CUDA) || defined(__USE_MPI_CUDA)
typedef float Scalar;
int rdf_gpu(float *x1, int n1, float *x2, int n2,
            float Lx, float Ly, float Lz, float binsize,
            float* g, int nbins, double &memory_allocated);
#else
typedef double Scalar;
#endif

using namespace std;

void factorize(int nprocs, int n1, int n2, int& nprocs_n1, int& nprocs_n2)
{
  int i, j;
  int per_proc_n1, per_proc_n2;
  int Smin = n1 + n2;
  for (i = 1; i <= nprocs; i++) {
    if (nprocs % i) continue;
    j = nprocs / i;
    per_proc_n1 = (int)(n1 / i);
    per_proc_n2 = (int)(n2 / j);
    int S = per_proc_n1 + per_proc_n2;
    if (S < Smin) {
      Smin = S;
      nprocs_n1 = i;
      nprocs_n2 = j;
    }
  }
}

void scan_dump(char* filename, int *nall, int &N, int type1, int type2)
{
  ifstream ifs;
  ifs.open(filename);
  if (!ifs.good()) return;
  
  int i;
  char line[256];
  int n = 0;
  for (i = 0; i < 9; i++) {
    ifs.getline(line, 256);
    if (i == 3) sscanf(line, "%d\n", &N);
  }

  if (N <= 0) {
    cout << "Invalid value of N = " << N << "\n";
    exit(1);
  }

  int maxarg=64;
  char** arg = new char* [maxarg];

  nall[0] = nall[1] = 0;
  for (i = 0; i < N; i++) {
    ifs.getline(line, 256);

    // parse the line
    int narg = 0;
    char *word = strtok(line," \t\n\r\f");
    while (word) {
      arg[narg++] = word;
      word = strtok(NULL," \t\n\r\f");
    }
    
    int type = atoi(arg[2]);
    //printf("narg  %d type = %d\n", narg, type);
    if (type == type1 || type1 < 0) nall[0]++;
    if (type == type2 || type2 < 0) nall[1]++;
  }

  ifs.close();

  delete [] arg;
}

#if defined(__USE_MPI_NOCUDA) || defined(__USE_MPI_CUDA) || defined(__USE_MPI_OPENMP)
void read_dump_mpi(char* filename, Scalar*& x1, Scalar*& x2, int &n1, int &n2, int &N,
                   Scalar& Lx, Scalar& Ly, Scalar& Lz, int type1, int type2, int *nall)
{
  int me=0, nprocs=1;
  MPI_Comm_rank(MPI_COMM_WORLD,&me);
  MPI_Comm_size(MPI_COMM_WORLD,&nprocs);

  if (nall[0] == 0 && nall[1] == 0) {
    if (me == 0) {
      printf("Scanning dump file..\n");
      scan_dump(filename, nall, N, type1, type2);
    }
    MPI_Bcast(nall,2,MPI_INT,0,MPI_COMM_WORLD);
  }

  // find the range of my data
  int nprocs_n1=1, nprocs_n2=1;
  factorize(nprocs, nall[0], nall[1], nprocs_n1, nprocs_n2);

  int coord_x = me % nprocs_n1;
  int coord_y = me / nprocs_n1;
  
  int per_proc_n1 = nall[0] / nprocs_n1;
  int n1start = coord_x * per_proc_n1;
  int n1end = n1start + per_proc_n1;
  if (coord_x == nprocs_n1-1) n1end = nall[0];
  
  int per_proc_n2 = nall[1] / nprocs_n2;
  int n2start = coord_y * per_proc_n2;
  int n2end = n2start + per_proc_n2;
  if (coord_y == nprocs_n2-1) n2end = nall[1];

  ifstream ifs;
  if (me == 0) {
    ifs.open(filename);
    if (!ifs.good()) return;
  }
  
  char line[256];
  double xlo, xhi, ylo, yhi, zlo, zhi;
  int n = 0;
  for (int i = 0; i < 9; i++) {
    if (me == 0) {
      ifs.getline(line, 256);
      n = strlen(line) + 1;
    }
    
    MPI_Bcast(&n,1,MPI_INT,0,MPI_COMM_WORLD);    
    MPI_Bcast(line,n,MPI_CHAR,0,MPI_COMM_WORLD);
    
    if (i == 3) sscanf(line, "%d\n", &N);
    else if (i == 5) {
      sscanf(line, "%lf %lf\n", &xlo, &xhi);
    } else if (i == 6) {
      sscanf(line, "%lf %lf\n", &ylo, &yhi);
    } else if (i == 7) {
      sscanf(line, "%lf %lf\n", &zlo, &zhi);
    }
  }

  if (N <= 0) {
    if (me == 0) 
      cout << "Invalid value of N = " << N << "\n";
    exit(1);
  }

  int nmax1=1000,nmax2=1000;
  x1 = (Scalar*) malloc(4*nmax1*sizeof(Scalar));
  x2 = (Scalar*) malloc(4*nmax2*sizeof(Scalar));

  Lx = xhi - xlo;
  Ly = yhi - ylo;
  Lz = zhi - zlo;

  int icount = 0;
  int jcount = 0;
  int id, type;
  double x, y, z;
  double data[6];
  int n1allcount=0, n2allcount=0;
  int maxarg=64;
  char** arg = new char* [maxarg];
  for (int i = 0; i < N; i++) {
    if (me == 0) {
      ifs.getline(line, 256);

      // parse the line
      int narg = 0;
      char *word = strtok(line," \t\n\r\f");
      while (word) {
        arg[narg++] = word;
        word = strtok(NULL," \t\n\r\f");
      }
      int id = atoi(arg[0]);
      int mol = atoi(arg[1]);
      type = atoi(arg[2]);
      double q = atof(arg[3]);
      if (type == type1 || type1 < 0) n1allcount++;
      if (type == type2 || type2 < 0) n2allcount++;
      x = atof(arg[4]);
      y = atof(arg[5]);
      z = atof(arg[6]);

      data[0] = n1allcount;
      data[1] = n2allcount;
      data[2] = type;
      data[3] = x;
      data[4] = y;
      data[5] = z;
    }
    
    MPI_Bcast(&data,6,MPI_DOUBLE,0,MPI_COMM_WORLD);

    n1allcount = (int)data[0];
    n2allcount = (int)data[1];
    type = (int)data[2];
    x = data[3];
    y = data[4];
    z = data[5];

    if (type == type1 || type1 < 0) {
      if (n1allcount > n1start && n1allcount <= n1end) {
        if (icount == nmax1) {
          nmax1 += DELTA;
          x1 = (Scalar*)realloc(x1, 4*nmax1*sizeof(Scalar));
        }
      
        x1[4*icount+0] = x;
        x1[4*icount+1] = y;
        x1[4*icount+2] = z;
        x1[4*icount+3] = type;
        icount++;
      }
    }
    
    if (type == type2 || type2 < 0) {
      if (n2allcount > n2start && n2allcount <= n2end) {
        if (jcount == nmax2) {
          nmax2 += DELTA;
          x2 = (Scalar*)realloc(x2, 4*nmax2*sizeof(Scalar));
        }
      
        x2[4*jcount+0] = x;
        x2[4*jcount+1] = y;
        x2[4*jcount+2] = z;
        x2[4*jcount+3] = type;
        jcount++;
      }
    }
  }

  n1 = icount;
  n2 = jcount;
  ifs.close();

  delete [] arg;

  // error checks
  if (n1allcount < nall[0] || n2allcount < nall[1]) {
    if (me == 0) 
      cout << "n1 (or n2) from command-line arguments greater than "
              "the actual number read in.\n";
    
    free(x1);
    free(x2);
    MPI_Finalize();
    exit(1);
  }
  
  int flag=0, flagall;
  flag &= (n1 - (n1end-n1start));
  flag &= (n2 - (n2end-n2start));
  MPI_Allreduce(&flag, &flagall, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  if (flag) {
    if (me == 0) {
      if (n1 - (n1end-n1start)) {
        cout << "Inconsistent values of n1: ";
        cout << n1 << "; " << (n1end-n1start) << " expected\n";
      }
      if (n2 - (n2end-n2start)) {
        cout << "Inconsistent values of n2: ";
        cout << n2 << "; " << (n2end-n2start) << " expected\n";
      }
    }

    free(x1);
    free(x2);
    MPI_Finalize();
    exit(1);
  }
}
#endif // defined(__USE_CUDA) || defined(__USE_MPI_CUDA)

void read_dump(char* filename, Scalar*& x1, Scalar*& x2, int &n1, int &n2, int &N,
               Scalar& Lx, Scalar& Ly, Scalar& Lz, int type1, int type2)
{
  ifstream ifs;
  ifs.open(filename);
  if (!ifs.good()) return;
  
  char line[256];
  int i;
  
  double xlo, xhi, ylo, yhi, zlo, zhi;
  for (i = 0; i < 9; i++) {
    ifs.getline(line, 256);
    if (i == 3) sscanf(line, "%d\n", &N);
    else if (i == 5) {
      sscanf(line, "%lf %lf\n", &xlo, &xhi);
    } else if (i == 6) {
      sscanf(line, "%lf %lf\n", &ylo, &yhi);
    } else if (i == 7) {
      sscanf(line, "%lf %lf\n", &zlo, &zhi);
    }
  }

  if (N <= 0) {
    cout << "Invalid value of N = " << N << "\n";
    exit(1);
  }
  
  int nmax1=1000,nmax2=1000;
  x1 = (Scalar*) malloc(4*nmax1*sizeof(Scalar));
  x2 = (Scalar*) malloc(4*nmax2*sizeof(Scalar));
  
  Lx = xhi - xlo;
  Ly = yhi - ylo;
  Lz = zhi - zlo;

  int icount = 0;
  int jcount = 0;
  int id, type;
  double x, y, z;
  
  int maxarg=6;
  char** arg = new char* [maxarg];
  for (i = 0; i < N; i++) {
    ifs.getline(line, 256);
    
    // parse the line
    int narg = 0;
    char *word = strtok(line," \t\n\r\f");
    while (word) {
      arg[narg++] = word;
      word = strtok(NULL," \t\n\r\f");
    }

    int id = atoi(arg[0]);
    int mol = atoi(arg[1]);
    type = atoi(arg[2]);
    double q = atof(arg[3]);
    x = atof(arg[4]);
    y = atof(arg[5]);
    z = atof(arg[6]);
    
    if (type == type1 || type1 < 0) {
      if (icount == nmax1) {
        nmax1 += DELTA;
        x1 = (Scalar*)realloc(x1, 4*nmax1*sizeof(Scalar));
      }
      
      x1[4*icount+0] = x;
      x1[4*icount+1] = y;
      x1[4*icount+2] = z;
      x1[4*icount+3] = type;
      icount++;
    }
    
    if (type == type2 || type2 < 0) {
      if (jcount == nmax2) {
        nmax2 += DELTA;
        x2 = (Scalar*)realloc(x2, 4*nmax2*sizeof(Scalar));
      }
      
      x2[4*jcount+0] = x;
      x2[4*jcount+1] = y;
      x2[4*jcount+2] = z;
      x2[4*jcount+3] = type;
      jcount++;
    }
  }

  n1 = icount;
  n2 = jcount;
  ifs.close();
}

void rdf_cpu(double *x1, int n1, double *x2, int n2, 
             double Lx, double Ly, double Lz, double binsize,
             float *g, int nbins)
{
  double Lx2 = 0.5*Lx;
  double Ly2 = 0.5*Ly;
  double Lz2 = 0.5*Lz;
  
  for (int i = 0; i < nbins; i++) g[i] = 0.0f;

  for (int i = 0; i < n1; i++) {
    double xtmp = x1[4*i+0];
    double ytmp = x1[4*i+1];
    double ztmp = x1[4*i+2];
    for (int j = 0; j < n2; j++) {
      double dx = xtmp - x2[4*j+0];
      double dy = ytmp - x2[4*j+1];
      double dz = ztmp - x2[4*j+2];
      if (dx <= -Lx2) dx += Lx;
      else if (dx >= Lx2) dx -= Lx;
      if (dy <= -Ly2) dy += Ly;
      else if (dy >= Ly2) dy -= Ly;
      if (dz <= -Lz2) dz += Lz;
      else if (dz >= Lz2) dz -= Lz;
      double dr = dx * dx + dy * dy + dz * dz;
      if (dr > EPSILON) {
        unsigned int ibin = (unsigned int)(sqrt(dr) / binsize);
        if (ibin >= 0 && ibin < nbins) 
          g[ibin] += 1.0f;
      }
    }
  }
}

#ifdef __USE_OPENMP
void rdf_openmp(double *x1, int n1, double *x2, int n2,
                double Lx, double Ly, double Lz, double binsize,
                float *g, int nbins, int nthreads)
{
  double Lx2 = 0.5f*Lx;
  double Ly2 = 0.5f*Ly;
  double Lz2 = 0.5f*Lz;
  
  float *g_rep = new float [nthreads*nbins];

  #pragma omp parallel
  {
  int me = omp_get_thread_num();
    
  #pragma omp for schedule(static)
  for (int i = 0; i < nbins*nthreads; i++) g_rep[i] = 0.0f;
    
  float *local_g = &g_rep[me*nbins];

  #pragma omp for schedule(static)
  for (int i = 0; i < n1; i++) {
    double xtmp = x1[4*i+0];
    double ytmp = x1[4*i+1];
    double ztmp = x1[4*i+2];
    for (int j = 0; j < n2; j++) {
      double dx = xtmp - x2[4*j+0];
      double dy = ytmp - x2[4*j+1];
      double dz = ztmp - x2[4*j+2];
      if (dx <= -Lx2) dx += Lx;
      else if (dx >= Lx2) dx -= Lx;
      if (dy <= -Ly2) dy += Ly;
      else if (dy >= Ly2) dy -= Ly;
      if (dz <= -Lz2) dz += Lz;
      else if (dz >= Lz2) dz -= Lz;
      double dr = dx * dx + dy * dy + dz * dz;
      if (dr > EPSILON) {
        unsigned int ibin = (unsigned int)(sqrt(dr) / binsize);
        if (ibin >= 0 && ibin < nbins) 
          local_g[ibin] += 1.0f;
      }
    }
  }

  #pragma omp for schedule(static)
  for (int i = 0; i < nbins; i++) {
    int offset = nthreads/2;
    while (offset > 0) {
      for (int j = 0; j < offset; j++)
        if (j + offset < nthreads)
          g_rep[j*nbins+i] += g_rep[(j+offset)*nbins+i];
      offset >>= 1;
    }
    g[i] = g_rep[i];
  }
  
  } // end parallel region
  
  delete [] g_rep;
}
#endif

#ifdef __USE_MPI_OPENMP
void rdf_mpi_openmp(double *x1, int n1, double *x2, int n2,
             double Lx, double Ly, double Lz, double binsize,
             float *g, int nbins, int nthreads)
{
  double Lx2 = 0.5*Lx;
  double Ly2 = 0.5*Ly;
  double Lz2 = 0.5*Lz;

  float *local_g = new float [nbins];
  for (int i = 0; i < nbins; i++) local_g[i] = 0.0f;
  
  float *g_rep = new float [nthreads*nbins];

  #pragma omp parallel
  {
  int me = omp_get_thread_num();
    
  #pragma omp for schedule(static)
  for (int i = 0; i < nbins*nthreads; i++) g_rep[i] = 0.0f;
    
  float *thread_g = &g_rep[me*nbins];

  #pragma omp for schedule(static) 
  for (int i = 0; i < n1; i++) {
    double xtmp = x1[4*i+0];
    double ytmp = x1[4*i+1];
    double ztmp = x1[4*i+2];
    for (int j = 0; j < n2; j++) {
      double dx = xtmp - x2[4*j+0];
      double dy = ytmp - x2[4*j+1];
      double dz = ztmp - x2[4*j+2];
      if (dx <= -Lx2) dx += Lx;
      else if (dx >= Lx2) dx -= Lx;
      if (dy <= -Ly2) dy += Ly;
      else if (dy >= Ly2) dy -= Ly;
      if (dz <= -Lz2) dz += Lz;
      else if (dz >= Lz2) dz -= Lz;
      double dr = dx * dx + dy * dy + dz * dz;
      if (dr > EPSILON) {
        unsigned int ibin = (unsigned int)(sqrt(dr) / binsize);
        if (ibin >= 0 && ibin < nbins) 
          thread_g[ibin] += 1.0f;
      }
    }
  }

  #pragma omp for schedule(static)
  for (int i = 0; i < nbins; i++) {
    int offset = nthreads/2;
    while (offset > 0) {
      for (int j = 0; j < offset; j++)
        if (j + offset < nthreads)
          g_rep[j*nbins+i] += g_rep[(j+offset)*nbins+i];
      offset >>= 1;
    }
    local_g[i] = g_rep[i];
  }
  
  } // end parallel region

  MPI_Allreduce(local_g, g, nbins, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
  
  delete [] local_g;
  delete [] g_rep;
}
#endif

#ifdef __USE_MPI_NOCUDA
void rdf_mpi(double *x1, int n1, double *x2, int n2,
             double Lx, double Ly, double Lz, double binsize,
             float *g, int nbins)
{
  double Lx2 = 0.5*Lx;
  double Ly2 = 0.5*Ly;
  double Lz2 = 0.5*Lz;
  
  float *local_g = new float [nbins];
  for (int i = 0; i < nbins; i++) local_g[i] = 0.0f;
  
  for (int i = 0; i < n1; i++) {
    double xtmp = x1[4*i+0];
    double ytmp = x1[4*i+1];
    double ztmp = x1[4*i+2];
    for (int j = 0; j < n2; j++) {
      double dx = xtmp - x2[4*j+0];
      double dy = ytmp - x2[4*j+1];
      double dz = ztmp - x2[4*j+2];
      if (dx <= -Lx2) dx += Lx;
      else if (dx >= Lx2) dx -= Lx;
      if (dy <= -Ly2) dy += Ly;
      else if (dy >= Ly2) dy -= Ly;
      if (dz <= -Lz2) dz += Lz;
      else if (dz >= Lz2) dz -= Lz;
      double dr = dx * dx + dy * dy + dz * dz;
      if (dr > EPSILON) {
        unsigned int ibin = (unsigned int)(sqrt(dr) / binsize);
        if (ibin >= 0 && ibin < nbins) 
          local_g[ibin] += 1.0f;
      }
    }
  }

  MPI_Allreduce(local_g, g, nbins, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
  
  delete [] local_g;
}
#endif

#ifdef __USE_MPI_CUDA
void rdf_mpi_gpu(Scalar *x1, int n1, Scalar *x2, int n2,
                 Scalar Lx, Scalar Ly, Scalar Lz, Scalar binsize,
                 float *g, int nbins, double &total_device_mem)
{
  float *local_g = new float [nbins];
  for (int i = 0; i < nbins; i++) local_g[i] = 0.0f;

  double memory_allocated;
  rdf_gpu(x1, n1, x2, n2, Lx, Ly, Lz, binsize, local_g, nbins, memory_allocated);
  MPI_Allreduce(local_g, g, nbins, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
  delete [] local_g;

  MPI_Allreduce(&memory_allocated, &total_device_mem, 1, 
    MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD); 
}
#endif

int main(int argc, char** argv)
{
  int me=0, nprocs=1;
#if defined(__USE_MPI_NOCUDA) || defined(__USE_MPI_CUDA) || defined(__USE_MPI_OPENMP)
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&me);
  MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
#endif

  if (argc < 4) {
    if (me == 0) cout << "Arguments: fileName type1 t1 [type2 t2] ...\n";
#if defined(__USE_MPI_NOCUDA) || defined(__USE_MPI_CUDA) || defined(__USE_MPI_OPENMP)
    MPI_Finalize();
#endif
    return 1;
  }
  
  char fileName[256];
  sprintf(fileName, "%s", argv[1]);
  int type1=-1, type2=-1;
  int nthreads = 1;
  int print_stat = 1;
  int nall[2];
  nall[0] = nall[1] = 0; 

  // parse the arguments
  int iarg = 2;
  while (iarg < argc) {
    if (strcmp(argv[iarg],"type1")==0) {
      type1 = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"n1")==0) {
      nall[0] = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"type2")==0) {
      type2 = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"n2")==0) {
      nall[1] = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"nthreads")==0) {
      nthreads = atoi(argv[iarg+1]);
      iarg += 2;
    } else if (strcmp(argv[iarg],"print_stat")==0) {
      print_stat = atoi(argv[iarg+1]);
      iarg += 2;
    } else {
      if (me == 0) cout << "Invalid argument: " << argv[iarg] << "\n";
      #if defined(__USE_MPI_NOCUDA) || defined(__USE_MPI_CUDA) || defined(__USE_MPI_OPENMP)
      MPI_Finalize();
      #endif
      return 1;
    }
  }

  // default values
  if (type2 < 0) {
    type2 = type1;
    nall[1] = nall[0];
  }

  Scalar *x1=0x0, *x2=0x0;
  int n1=0, n2=0, N=0;
  Scalar Lx, Ly, Lz;
  double elapsed_time;

#if defined(__USE_MPI_NOCUDA) || defined(__USE_MPI_CUDA) || defined(__USE_MPI_OPENMP)
  read_dump_mpi(fileName, x1, x2, n1, n2, N,
                Lx, Ly, Lz, type1, type2, nall);
#else
  read_dump(fileName, x1, x2, n1, n2, N,
            Lx, Ly, Lz, type1, type2);
  nall[0] = n1;
  nall[1] = n2;
#endif

  int nbins = 256;
  Scalar binsize = (Scalar)Lx/2.0/nbins;
  float *g = (float*) malloc(nbins*sizeof(float));

  double factor = 4.0/3.0*4.0*atan(1.0)*pow((double)binsize,3.0);
  double box_vol = Lx * Ly * Lz;
  // number of unique pairs (for two identical sets)
  double npairs = (double)nall[0] * (double)(nall[1] - 1);  
  // for two non-overlapping sets
  if (type1 != type2) 
    npairs = (double)nall[0] * (double)nall[1]; 
  
  // print out statistics
  if (print_stat && me == 0) {
    cout << "n1      = " << nall[0] << "\n";
    cout << "n2      = " << nall[1] << "\n";
    cout << "npairs  = " << npairs << "\n";
    cout << "box     = " << Lx << " " << Ly << " " << Lz << "\n";
    cout << "binsize = " << binsize << "\n";
  }

  double tstart, telapsed;
  if (print_stat && me == 0) {
    cout << "Computing..\n";
  }

#ifdef __USE_SINGLE_CPU
  clock_t start, elapsed;
  start = clock();
  rdf_cpu(x1, n1, x2, n2, Lx, Ly, Lz, binsize, g, nbins);
  elapsed = clock()-start;
  if (print_stat) {
    printf("Compute time of %f seconds for %g pairs\n", 
      (double)telapsed/CLOCKS_PER_SEC, nprocs, npairs);
  }
#endif

#ifdef __USE_OPENMP
  omp_set_num_threads(nthreads);
	tstart = omp_get_wtime();
  rdf_openmp(x1, n1, x2, n2, Lx, Ly, Lz, binsize, g, nbins, nthreads);
  telapsed = omp_get_wtime()-tstart;
  if (print_stat && me == 0) {
    printf("Compute time of %f seconds on %d OpenMP thread(s) for %g pairs\n", 
      telapsed/nprocs, nthreads, npairs);
  }
#endif

#ifdef __USE_MPI_OPENMP
  omp_set_num_threads(nthreads);
  double tmp;
  tstart = MPI_Wtime();
  rdf_mpi_openmp(x1, n1, x2, n2, Lx, Ly, Lz, binsize, g, nbins, nthreads);
  telapsed = MPI_Wtime()-tstart;

  MPI_Allreduce(&telapsed,&tmp,1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  if (print_stat && me == 0) {
    printf("Compute time of %f seconds on %d MPI proc(s) x %d OpenMP thread(s) for %g pairs\n", 
      telapsed/nprocs, nprocs, nthreads, npairs);
  }
#endif

#ifdef __USE_CUDA
  double memory_allocated;
  cloct_t start, elapsed;
  start = clock();
  int value = rdf_gpu(x1, n1, x2, n2, Lx, Ly, Lz, binsize, g, nbins, memory_allocated);
  elapsed = clock()-start;
  if (value == -1) printf("Error with device memory allocation\n");
  else if (value == -2) printf("Error with host-device memory copy\n");
  else if (value == -3) printf("Error with device-host memory copy\n");
  else if (value == -4) printf("Error with device memory deallocation\n");

  if (print_stat) {
    printf("Memory allocated on the device: %f Mbytes\n", 
      memory_allocated/1048576.0);
    printf("Compute time of %f seconds for %g pairs\n", 
      (double)telapsed/CLOCKS_PER_SEC, nprocs, npairs);
  }
#endif

#ifdef __USE_MPI_NOCUDA
  double tmp;
  tstart = MPI_Wtime();
  rdf_mpi(x1, n1, x2, n2, Lx, Ly, Lz, binsize, g, nbins);
  telapsed = MPI_Wtime()-tstart;

  MPI_Allreduce(&telapsed,&tmp,1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  if (print_stat && me == 0) {
    printf("Compute time of %f seconds on %d MPI proc(s) for %g pairs\n", 
      telapsed/nprocs, nprocs, npairs);
  }
#endif

#ifdef __USE_MPI_CUDA
  double total_device_mem, tmp;

  tstart = MPI_Wtime();
  rdf_mpi_gpu(x1, n1, x2, n2, Lx, Ly, Lz, binsize, g, nbins, total_device_mem);
  telapsed = MPI_Wtime()-tstart;

  MPI_Allreduce(&telapsed,&tmp,1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  if (print_stat && me == 0) {
    printf("Average memory allocated on the device: %f Mbytes\n", 
      total_device_mem/nprocs/1048576.0);
    printf("Compute time of %f seconds on %d MPI proc(s) for %g pairs\n", 
      telapsed/nprocs, nprocs, npairs);
  }
#endif
  if (print_stat && me == 0) {
    cout << "Writing to file..\n";
  }

  ofstream ofs;
  ofs.open("rdf.txt");
  ofs << "r g(r) ncount\n";
  for (int i = 0; i < nbins; i++) {
    double r = (i + 0.5) * binsize;
    double dv = factor * (pow((double)(i+1), 3.0) - pow((double)i, 3.0));
    double gr = (double)g[i] / (npairs * dv / box_vol);
    ofs << r << " " << gr << " " << g[i] << "\n";
  }
    
  ofs.close();

  if (print_stat && me == 0) {
    cout << "Cleaning up..\n";
  }

  if (x1) free(x1);
  if (x2) free(x2);
  free(g);

#if defined(__USE_MPI_NOCUDA) || defined(__USE_MPI_CUDA) || defined(__USE_MPI_OPENMP)
  MPI_Finalize();
#endif

  return 0;
}
