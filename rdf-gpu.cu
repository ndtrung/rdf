/*
  CUDA compiled code for pair correlation function
  between two coordinate sets
    
  Contact: Trung Dac Nguyen (ORNL), nguyentd@ornl.gov
*/

#include <stdio.h>
#include "cuda.h"
#include "cuda_runtime.h"

#define EPSILON 0.01f
 
// Error handling macro
#define CUDA_CHECK(call) \
    if((call) != cudaSuccess) { \
        cudaError_t err = cudaGetLastError(); \
        cerr << "CUDA error calling \""#call"\", code is " << err << endl; }

extern __shared__ float4 shared_mem[];

__global__ void kernel_rdf(float4 * __restrict__ x1, int n1,
                           float4 * __restrict__ x2, int n2,
                           float Lx, float Ly, float Lz, float binsize,
                           float * __restrict__ g_rep,
                           int nbins, int warp_size)
{
  unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;

  float4 pos_i;
  float dx, dy, dz;
  float Lx2 = 0.5f*Lx;
  float Ly2 = 0.5f*Ly;
  float Lz2 = 0.5f*Lz;
  float *myhist = &g_rep[blockIdx.x*nbins];
  int num_warps = blockDim.x / warp_size;
  int idx_hist = threadIdx.x / warp_size;

  float4 *shared_pos_j = shared_mem;
  float *perblock_hist = (float*)(&shared_mem[blockDim.x]);
  // perblock_hist: num_warps by nbins
  // blockDim.x = num_warps * warp_size
  for (int i = 0; i < (num_warps*nbins/blockDim.x); i++) 
    perblock_hist[threadIdx.x + i * blockDim.x] = 0;
  
  if (idx < n1) pos_i = x1[idx];
  else pos_i = make_float4(0.0f,0.0f,0.0f,0.0f);

  // unroll the loop through n2 by stride
  for (int start = 0; start < n2; start += blockDim.x) {
    // read x2 from global memory
    // and write to the shared memory array
    if (start+threadIdx.x < n2) {
      float4 tmp = x2[start+threadIdx.x];
      shared_pos_j[threadIdx.x].x = tmp.x;
      shared_pos_j[threadIdx.x].y = tmp.y;
      shared_pos_j[threadIdx.x].z = tmp.z;
      shared_pos_j[threadIdx.x].w = 1.0f;
    } else {
      shared_pos_j[threadIdx.x] = make_float4(0.0f,0.0f,0.0f,0.0f);
    }
      
    __syncthreads();

    // read from the shared memory array
    if (idx < n1) {
      for (int j = 0; j < blockDim.x; j++) {
        float4 pos_j = shared_pos_j[j];
        if (pos_j.w > 0.0f) {
          dx = pos_i.x-pos_j.x;
          dy = pos_i.y-pos_j.y;
          dz = pos_i.z-pos_j.z;
          if (dx <= (-1.0f)*Lx2) dx += Lx;
          else if (dx >= Lx2) dx -= Lx;
          if (dy <= (-1.0f)*Ly2) dy += Ly;
          else if (dy >= Ly2) dy -= Ly;
          if (dz <= (-1.0f)*Lz2) dz += Lz;
          else if (dz >= Lz2) dz -= Lz;
          float dr = dx * dx + dy * dy + dz * dz;
          if (dr > EPSILON) {
            unsigned int ibin = (unsigned int)(sqrt(dr) / binsize);
            if (ibin < nbins)
              atomicAdd(&perblock_hist[idx_hist*nbins+ibin],1.0f);
          }
        }
      }
    }
    __syncthreads();
  }

  for (int ibin = threadIdx.x; ibin < nbins; ibin += blockDim.x) {
    float sum = 0.0f;
    for(int j = 0; j < num_warps; j++)
      sum += perblock_hist[j * nbins + ibin];
    myhist[ibin] = sum;
  }
}

__global__ void kernel_reduce(float * __restrict__ g_rep,
                              int nbins, int x1_num_blocks,
                              float * __restrict__ g)
{
  for (int start = 0; start < x1_num_blocks; start += blockDim.x) {
    unsigned int idx = start + threadIdx.x;
    int offset = blockDim.x >> 1;
    while (offset > 0) {
      if (threadIdx.x < offset) {
        if (idx + offset < x1_num_blocks) {
          #pragma unroll
          for (int j = 0; j < nbins; j++) 
            g_rep[idx*nbins+j] += g_rep[(idx+offset)*nbins+j];
        }
      }
      offset >>= 1;
      __syncthreads();
    }
    
    if (threadIdx.x == 0) {
      #pragma unroll
      for (int j = 0; j < nbins; j++) {
        g[j] += g_rep[start*nbins+j];
      }
    }
  }
}

void showDeviceInfo()
{
  int deviceCount = 0;
  cudaError_t error = cudaGetDeviceCount(&deviceCount);
  if (error != cudaSuccess) {
    printf( "cudaGetDeviceCount returned %d\n-> %s\n", 
      (int)error, cudaGetErrorString(error) );
    return;
  }

  for (int dev = 0; dev < deviceCount; dev++) {
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, dev);
    printf("\nDevice %d: \"%s\"\n", dev, deviceProp.name);
    printf("  CUDA Compute Capability:                       %d.%d\n", 
      deviceProp.major, deviceProp.minor);

    printf("  Total amount of global memory:                 %.0f MBytes (%llu bytes)\n", 
      (float)deviceProp.totalGlobalMem/1048576.0f, 
      (unsigned long long) deviceProp.totalGlobalMem);
    printf("  Number of multiprocessors:                     %d\n", 
      deviceProp.multiProcessorCount);
    printf("  GPU Clock Speed:                               %.2f GHz\n", 
      deviceProp.clockRate * 1e-6f);
    printf("  Total amount of constant memory:               %u bytes\n", 
      deviceProp.totalConstMem); 
    printf("  Total amount of shared memory per block:       %u bytes\n", 
      deviceProp.sharedMemPerBlock);
    printf("  Total number of registers available per block: %d\n", 
      deviceProp.regsPerBlock);
    printf("  Warp size:                                     %d\n", 
      deviceProp.warpSize);
    printf("  Maximum number of threads per block:           %d\n", 
      deviceProp.maxThreadsPerBlock);
  }
}

int rdf_gpu(float *x1, int n1, float *x2, int n2,
            float Lx, float Ly, float Lz, float binsize, 
            float* g, int nbins, double& mem_allocated)
{
  float4 *d_x1=0x0, *d_x2=0x0;
  float *d_g=0x0, *d_g_rep=0x0;
  cudaError_t error;

  unsigned int x1_block_size = 256;
  unsigned int x1_num_blocks = (unsigned int)(n1/x1_block_size) + 1;

  // Allocate memory on the device
  mem_allocated = 0.0f;

  cudaStream_t stream1, stream2;
  cudaStreamCreate(&stream1);
  cudaStreamCreate(&stream2);

  error = cudaMalloc((void**)&d_x1, n1*sizeof(float4));
  if (error != cudaSuccess) return -1;

  error = cudaMalloc((void**)&d_x2, n2*sizeof(float4));
  if (error != cudaSuccess) return -1;
  
  error = cudaMalloc((void**)&d_g_rep, x1_num_blocks*nbins*sizeof(float));
  if (error != cudaSuccess) return -1;

  error = cudaMalloc((void**)&d_g, nbins*sizeof(float));
  if (error != cudaSuccess) return -1;
  
  mem_allocated = (n1+n2)*sizeof(float4) + x1_num_blocks*nbins*sizeof(float) + 
    nbins*sizeof(float);
 
  // Copy input data from host to device
  error = cudaMemcpyAsync(d_x1, (float4*)x1, n1*sizeof(float4),
    cudaMemcpyHostToDevice, stream1);
  if (error != cudaSuccess) return -2;

  error = cudaMemcpyAsync(d_x2, (float4*)x2, n2*sizeof(float4),
    cudaMemcpyHostToDevice, stream2);
  if (error != cudaSuccess) return -2;

  cudaMemsetAsync(d_g_rep, 0, x1_num_blocks*nbins*sizeof(float), stream1);
  cudaMemsetAsync(d_g, 0, nbins*sizeof(float), stream2); 

  // Wait until all streams on the device complete

  cudaDeviceSynchronize();

  // Launch the kernels

  int warp_size = 32;
  int num_warps = x1_block_size / warp_size;
  size_t shared_mem_size = x1_block_size*sizeof(float4) + 
    num_warps*nbins*sizeof(float);

//#define __CUDA_TIMING
#ifdef __CUDA_TIMING
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  cudaEventRecord(start, 0);
#endif

  kernel_rdf<<<x1_num_blocks,x1_block_size,shared_mem_size>>>(d_x1, n1, d_x2, n2, 
    (float)Lx, (float)Ly, (float)Lz, (float)binsize, d_g_rep, nbins, warp_size);

#ifdef __CUDA_TIMING
  cudaEventRecord(stop, 0);
  cudaEventSynchronize(stop);
  float elapsedTime;
  cudaEventElapsedTime(&elapsedTime, start, stop);
  printf("RDF kernel time: %3.1f ms\n", elapsedTime);
  cudaEventDestroy(start);
  cudaEventDestroy(stop);
#endif

//#define __DEBUG
#ifdef __DEBUG

  float *h_g_rep = (float*)malloc(x1_num_blocks*nbins*sizeof(float));
  error = cudaMemcpy(h_g_rep, d_g_rep, x1_num_blocks*nbins*sizeof(float), 
    cudaMemcpyDeviceToHost);
  if (error != cudaSuccess) {
    printf( "cudaMemcpy returned %d:  %s\n", (int)error, cudaGetErrorString(error) );
    return -3;
  }

  mem_allocated += x1_num_blocks*nbins*sizeof(float);

  for (int ibin = 0; ibin < nbins; ibin++) {
    g[ibin] = 0.0f;
    for (int j = 0; j < x1_num_blocks; j++)
      g[ibin] += h_g_rep[j*nbins+ibin];
  }
  free(h_g_rep);

#else

  // reduce the histograms
  unsigned int block_size = 256;
  unsigned int num_blocks = 1;
  kernel_reduce<<<num_blocks,block_size>>>(d_g_rep, nbins, x1_num_blocks, d_g);
  
  // Copy results from device back to host
  error = cudaMemcpy(g, d_g, nbins*sizeof(float), cudaMemcpyDeviceToHost);
  if (error != cudaSuccess) return -3;

#endif

  // Deallocate memory on the device

  error = cudaFree(d_x1);
  if (error != cudaSuccess) return -4;

  error = cudaFree(d_x2);
  if (error != cudaSuccess) return -4;

  error = cudaFree(d_g);
  if (error != cudaSuccess) return -4;

  error = cudaFree(d_g_rep);
  if (error != cudaSuccess) return -4;
 
  return 0;
}

