This software package is used to compute the radial distribution function (RDF) and structure factor based on the RDF of a N-particle system.
The software can be run with various acceleration configurations including
multihthreading (OpenMP), message-passing interface (MPI), GPU (with NVIDIA) CUDA), and their combinations (MPI/OpenMP and MPI/CUDA).
The strong and weak scaling performances of the code are to be published.

### Compilation and build ###

* Prerequisites: make, C++ compilers, MPI libraries (optional), CUDA Toolkit (optional)
* Choose the Makefile.foo for the acceleration platform available, e.g. Makefile.mpi_cuda
* Modify the Makefile.foo to match with the system-specific settings (i.e. compilers, paths)
* Build: make -f Makefile.foo -j4
* See the header of the source rdf-mpi.cpp for more detailed instructions.

### Contact ###

The code is developed by Trung Nguyen (ndactrung at gmail dot com). The software is provided as-is, and the author does not hold any responsibility
with regards to the use of the code in any study.